import React, { useEffect, useState } from "react";
import styles from "./index.module.css";
import data from "../../data.json";
import { useRouter } from "next/router";
import Head from "next/head";
import Footer from "../../Components/Footer/footer";
import { CaretRightOutlined } from "@ant-design/icons";
import Link from "next/link";
import Image from "next/image";
import { DownOutlined, UpOutlined } from "@ant-design/icons";

export default function Details(props: any) {
  const { slug } = props;
  const [Loading, setLoading] = useState(false);
  const router = useRouter();
  const [show, setShow] = useState(false);
  const [title, setTitle] = useState("");

  const filter = data.category.filter((x) => x.slug == slug);

  useEffect(() => {
    if (filter.length === 0) {
      router.push("/");
    }
  }, [filter]);

  setTimeout(() => {
    setLoading(false);
  }, 1500);

  if (Loading) {
    return null;
  }

  const item = filter && filter[0];

  const spand = (title: string) => {
    setTitle(title);
    setShow(!show);
  };

  return (
    <>
      <div className={styles.content_details}>
        <Head>
          <title>{item && item.name} | El Bosque Encantado</title>
          <meta
            name="description"
            content="El bosque encantado Burgos, el bar de moda tu ricón perfecto para crear momentos"
          />
        </Head>
        <div className="bg_header">
          <Link href="/">
            <a>
              <Image
                width={800}
                height={250}
                src="/images/logo.png"
                alt="El Bosque Encantado"
              />
            </a>
          </Link>
        </div>

        <div className={styles.info_cont}>
          <h1>{item && item.name}</h1>
          <span>{item && item.description}</span>

          <div className={styles.product_cont}>
            {item &&
              item.subCategory?.map((prod: any, i) => {
                return (
                  <div key={i}>
                    <div
                      style={{
                        cursor: "pointer",
                        display: "flex",
                        justifyContent: "flex-start",
                        alignItems: "center",
                      }}
                      onClick={() => spand(show ? "" : prod.name)}
                    >
                      <div>
                      <h2>{prod.name}</h2>
                      {item.subCategory.length <= 2 || show && title === prod.name ? <h4 className={styles.description}>{prod.description}</h4> : null}
                      
                      </div>
                      {
                        item.subCategory.length <= 2 || show && title === prod.name ? <UpOutlined
                        style={{
                          color: "#cc6c9c",
                          fontSize: 20,
                          marginLeft: 20,
                          marginTop: 10
                        }}
                      /> : <DownOutlined
                      style={{
                        color: "#cc6c9c",
                        fontSize: 20,
                        marginLeft: 20,
                        marginTop: 10
                      }}
                    />
                      }
                    </div>
                    
                    {item.subCategory.length <= 2 || show && title === prod.name ? (
                      <>
                        {prod.product.map((p: any, y: any) => {
                          return (
                            <ul key={y}>
                              <li>
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <p>
                                    <CaretRightOutlined
                                      style={{ color: "#cc6c9c", fontSize: 20 }}
                                    />
                                    {p.name}
                                  </p>
                                  {p.price ? (
                                    <>
                                      <span
                                        style={{
                                          color: "#cc6c9c",
                                          marginLeft: 20,
                                        }}
                                      >
                                        {p.price}
                                      </span>
                                    </>
                                  ) : null}
                                </div>
                                {p.description ? (
                                  <>
                                    <span>{p.description}</span>
                                    <br />
                                  </>
                                ) : null}
                              </li>
                            </ul>
                          );
                        })}
                      </>
                    ) : null}
                  </div>
                );
              })}
          </div>
        </div>
        <Footer />
      </div>
      <style jsx>
        {`
          .bg_header {
            width: 100%;
            height: 250px;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            position: relative;
            text-align: center;
            justify-content: center;
            align-items: center;
            display: flex;
            background-color: #cc6c9c;
          }

          .bg_header img {
            width: 50%;
          }

          @media (max-width: 768px) {
            .bg_header img {
              width: 350px;
            }
          }
        `}
      </style>
    </>
  );
}
