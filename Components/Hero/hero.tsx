import styles from "./hero.module.css";
import Header from "../Header/index";

export default function hero() {
  return (
    <div className={styles.hero_image}>
      <div className={styles.hero_text}>
        <h1 className={styles.title}>Bienvenidos a</h1>
        <Header />
        <p className={styles.subtitle}>
          Tu rincón perfecto para crear momentos
        </p>
      </div>
    </div>
  );
}
