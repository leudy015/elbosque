import React from "react";
import styles from "./contact.module.css";
import Map from "../Maps";
import { StarFilled } from "@ant-design/icons";
import Image from "next/image";

export default function Partidos() {
  return (
    <div className={styles.contact_cont}>
      <h1 className={styles.titulo}>Contacto</h1>
      <div className={styles.contact}>
        <div style={{ borderRadius: 20 }}>
          <Map
            height={200}
            width={400}
            lat={42.3428487}
            lgn={-3.6979873}
            title="El Bosque encantado"
          />
        </div>
        <div>
          <h1>Calle San Juan, 30</h1>
          <p>09004, Burgos</p>
          <h2 style={{ marginTop: 30 }}>Horario</h2>
          <span>Lunes - Viernes desde las 9:00</span> <br />
          <span>Sáb - Dom y festivos desde las 10:00</span> <br />
          <span>Tel: +34 947 26 12 66</span>
        </div>
      </div>

      <div className={styles.review}>
        <Image
          width={250}
          height={120}
          src="/images/google_reviews.png"
          alt="Valoraciones"
        />
        <div>
          <StarFilled style={{ color: "#fadb14" }} />
          <StarFilled style={{ color: "#fadb14" }} />
          <StarFilled style={{ color: "#fadb14" }} />
          <StarFilled style={{ color: "#fadb14" }} />
          <StarFilled style={{ color: "#fadb14" }} /> 4.5 / 575
        </div>
      </div>
    </div>
  );
}
