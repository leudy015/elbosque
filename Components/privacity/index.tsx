import styles from "./index.module.css";
import Footer from "../Footer/footer";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";

export default function Cookies() {
  return (
    <div style={{ backgroundColor: "#f4f5f5" }}>
      <Head>
        <title>Política de privacidad | El Bosque Encantado</title>
        <meta
          property="og:title"
          content="Política de privacidad | El Bosque Encantado"
          key="title"
        />
      </Head>
      <Link href="/">
        <a className={styles.logo}>
          <Image width={400} height={140} src="/images/logo.png" alt="" />
        </a>
      </Link>
      <div className={styles.containers_hepls}>
        <div className={styles.entry_content}>
          <blockquote className={styles.cookies}>
            <p>
              <strong>
                Protección de datos de carácter personal según la LOPD
              </strong>
            </p>
            <p>
              en aplicación de la normativa vigente en materia de protección de
              datos de carácter personal, informa que los datos personales que
              se recogen a través de los formularios del Sitio web:
              https://elbosqueencantadoburgos.com, se incluyen en los ficheros
              automatizados específicos de usuarios de los servicios.
            </p>
            <p>
              La recogida y tratamiento automatizado de los datos de carácter
              personal tiene como finalidad el mantenimiento de la relación
              comercial y el desempeño de tareas de información, formación,
              asesoramiento y otras actividades propias de infromación. Estos
              datos únicamente serán cedidos a aquellas entidades que sean
              necesarias con el único objetivo de dar cumplimiento a la
              finalidad anteriormente expuesta.
            </p>
            <p>
              El Bosque Encantado adopta las medidas necesarias para garantizar
              la seguridad, integridad y confidencialidad de los datos conforme
              a lo dispuesto en el Reglamento (UE) 2016/679 del Parlamento
              Europeo y del Consejo, de 27 de abril de 2016, relativo a la
              protección de las personas físicas en lo que respecta al
              tratamiento de datos personales y a la libre circulación de los
              mismos. El usuario podrá en cualquier momento ejercitar los
              derechos de acceso, oposición, rectificación y cancelación
              reconocidos en el citado Reglamento (UE). El ejercicio de estos
              derechos puede realizarlo el propio usuario a través de email a:
              contacto@elbosqueencantadoburgos.com o en la dirección: Calle San
              Juan 31, Bajo, 09004, Burgos El usuario manifiesta que todos los
              datos facilitados por él son ciertos y correctos, y se compromete
              a mantenerlos actualizados, comunicando los cambios a El Bosque
              Encantado
            </p>
            <p>
              <strong>
                Finalidad del tratamiento de los datos personales:
              </strong>
            </p>
            <p>¿Con qué finalidad trataremos tus datos personales?</p>
            <p>
              En El Bosque Encantado, trataremos tus datos personales recabados
              a través del Sitio Web: https://elbosqueencantadoburgos.com, con
              las siguientes finalidades: 1. En caso de contratación de los
              bienes y servicios ofertados a través de El Bosque Encantado, para
              mantener la relación contractual, así como la gestión,
              administración, información, prestación y mejora del servicio.
            </p>

            <strong>
              ¿Por cuánto tiempo se conservan los datos personales recabados?
            </strong>

            <p>
              Los datos personales proporcionados se conservarán mientras se
              mantenga la relación comercial o no solicites su supresión y
              durante el plazo por el cuál pudieran derivarse responsabilidades
              legales por los servicios prestados.
            </p>

            <strong>Legitimación:</strong>

            <p>
              El tratamiento de tus datos se realiza con las siguientes bases
              jurídicas que legitiman el mismo:
            </p>

            <ul>
              <li>
                <p>
                  La solicitud de información y/o la contratación de los
                  servicios de El Bosque Encantado, cuyos términos y condiciones
                  se pondrán a tu disposición en todo caso, de forma previa a
                  una eventual contratación.
                </p>
              </li>

              <li>
                <p>
                  El consentimiento libre, específico, informado e inequívoco,
                  en tanto que te informamos poniendo a tu disposición la
                  presente política de privacidad, que tras la lectura de la
                  misma, en caso de estar conforme, puedes aceptar mediante una
                  declaración o una clara acción afirmativa, como el marcado de
                  una casilla dispuesta al efecto.
                </p>
              </li>
            </ul>
            <p>
              En caso de que no nos facilites tus datos o lo hagas de forma
              errónea o incompleta, no podremos atender tu solicitud, resultando
              del todo imposible proporcionarte la información solicitada o
              llevar a cabo la contratación de los servicios.
            </p>

            <strong>Destinatarios:</strong>

            <p>
              Los datos no se comunicarán a ningún tercero ajeno a salvo
              obligación legal. Como encargados de tratamiento, tenemos
              contratados a los siguientes proveedores de servicios, habiéndose
              comprometido al cumplimiento de las disposiciones normativas, de
              aplicación en materia de protección de datos, en el momento de su
              contratación:
            </p>

            <ul>
              <li>
                <p>
                  (ENCARGADO) Mariam Huerto, con domicilio en Calle San Juan 31,
                  Bajo, 09004, Burgos, NIF/CIF nº 71278026-V, presta servicios
                  de El Bosque Encantado.  Puede consultar la política de
                  privacidad y demás aspectos legales de la compañía en el
                  siguiente enlace: https://elbosqueencantadoburgos.com/
                </p>
              </li>
            </ul>
            <p>
              <strong>Datos recopilados por usuarios de los servicios</strong>
            </p>
            <p>
              En los casos en que el usuario incluya ficheros con datos de
              carácter personal en los servidores de alojamiento compartido,
              https://elbosqueencantadoburgos.com no se hace responsable del
              incumplimiento por parte del usuario del RGPD.
            </p>

            <strong>Retención de datos en conformidad a la LSSI</strong>

            <p>
              https://elbosqueencantadoburgos.com informa de que, como prestador
              de servicio de alojamiento de datos y en virtud de lo establecido
              en la Ley 34/2002 de 11 de julio de Servicios de la Sociedad de la
              Información y de Comercio Electrónico (LSSI), retiene por un
              periodo máximo de 12 meses la información imprescindible para
              identificar el origen de los datos alojados y el momento en que se
              inició la prestación del servicio. La retención de estos datos no
              afecta al secreto de las comunicaciones y sólo podrán ser
              utilizados en el marco de una investigación criminal o para la
              salvaguardia de la seguridad pública, poniéndose a disposición de
              los jueces y/o tribunales o del Ministerio que así los requiera.
            </p>
            <p>
              La comunicación de datos a las Fuerzas y Cuerpos del Estado se
              hará en virtud a lo dispuesto en la normativa sobre protección de
              datos personales.
            </p>

            <p>
              <strong>
                Derechos propiedad intelectual
                https://elbosqueencantadoburgos.com{" "}
              </strong>
            </p>
            <p>
              https://elbosqueencantadoburgos.com es titular de todos los
              derechos de autor, propiedad intelectual, industrial, "know how" y
              cuantos otros derechos guardan relación con los contenidos del
              sitio web https://elbosqueencantadoburgos.com y los servicios
              ofertados en el mismo, así como de los programas necesarios para
              su implementación y la información relacionada.
            </p>
            <p>
              No se permite la reproducción, publicación y/o uso no
              estrictamente privado de los contenidos, totales o parciales, del
              sitio web https://elbosqueencantadoburgos.com sin el
              consentimiento previo y por escrito.
            </p>
            <p>
              <strong>Propiedad intelectual del software</strong>
            </p>
            <p>
              El usuario debe respetar los programas de terceros puestos a su
              disposición por https://elbosqueencantadoburgos.com, aun siendo
              gratuitos y/o de disposición pública.
            </p>
            <p>
              https://elbosqueencantadoburgos.com dispone de los derechos de
              explotación y propiedad intelectual necesarios del software.
            </p>

            <p>
              El usuario no adquiere derecho alguno o licencia por el servicio
              contratado, sobre el software necesario para la prestación del
              servicio, ni tampoco sobre la información técnica de seguimiento
              del servicio, excepción hecha de los derechos y licencias
              necesarios para el cumplimiento de los servicios contratados y
              únicamente durante la duración de los mismos.
            </p>

            <p>
              Para toda actuación que exceda del cumplimiento del contrato, el
              usuario necesitará autorización por escrito por parte de
              https://elbosqueencantadoburgos.com, quedando prohibido al usuario
              acceder, modificar, visualizar la configuración, estructura y
              ficheros de los servidores propiedad de
              https://elbosqueencantadoburgos.com, asumiendo la responsabilidad
              civil y penal derivada de cualquier incidencia que se pudiera
              producir en los servidores y sistemas de seguridad como
              consecuencia directa de una actuación negligente o maliciosa por
              su parte.
            </p>
            <strong>Propiedad intelectual de los contenidos alojados</strong>
            <p>
              Se prohíbe el uso contrario a la legislación sobre propiedad
              intelectual de los servicios prestados por
              https://elbosqueencantadoburgos.com y, en particular de:
            </p>

            <ul>
              <li>
                <p>
                  La utilización que resulte contraria a las leyes españolas o
                  que infrinja los derechos de terceros.
                </p>
              </li>
              <li>
                <p>
                  La publicación o la transmisión de cualquier contenido que, a
                  juicio de https://elbosqueencantadoburgos.com, resulte
                  violento, obsceno, abusivo, ilegal, racial, xenófobo o
                  difamatorio.
                </p>
              </li>
              <li>
                <p>
                  Los cracks, números de serie de programas o cualquier otro
                  contenido que vulnere derechos de la propiedad intelectual de
                  terceros.
                </p>
              </li>
              <li>
                <p>
                  La recogida y/o utilización de datos personales de otros
                  usuarios sin su consentimiento expreso o contraviniendo lo
                  dispuesto en Reglamento (UE) 2016/679 del Parlamento Europeo y
                  del Consejo, de 27 de abril de 2016, relativo a la protección
                  de las personas físicas en lo que respecta al tratamiento de
                  datos personales y a la libre circulación de los mismos.
                </p>
              </li>
              <li>
                <p>
                  La utilización del servidor de correo del dominio y de las
                  direcciones de correo electrónico para el envío de correo
                  masivo no deseado.
                </p>
              </li>
            </ul>

            <p>
              El usuario tiene toda la responsabilidad sobre el contenido de su
              web, la información transmitida y almacenada, los enlaces de
              hipertexto, las reivindicaciones de terceros y las acciones
              legales en referencia a propiedad intelectual, derechos de
              terceros y protección de menores.
            </p>
            <p>
              El usuario es responsable respecto a las leyes y reglamentos en
              vigor y las reglas que tienen que ver con el funcionamiento del
              servicio online, comercio electrónico, derechos de autor,
              mantenimiento del orden público, así como principios universales
              de uso de Internet.
            </p>
            <p>
              El usuario indemnizará a https://elbosqueencantadoburgos.com por
              los gastos que generara la imputación de alguna causa cuya
              responsabilidad fuera atribuible al usuario, incluidos honorarios
              y gastos de defensa jurídica, incluso en el caso de una decisión
              judicial no definitiva.
            </p>
            <strong>Protección de la información alojada</strong>
            <p>
              https://elbosqueencantadoburgos.com realiza copias de seguridad de
              los contenidos alojados en sus servidores, sin embargo no se
              responsabiliza de la pérdida o el borrado accidental de los datos
              por parte de los usuarios. De igual manera, no garantiza la
              reposición total de los datos borrados por los usuarios, ya que
              los citados datos podrían haber sido suprimidos y/o modificados
              durante el periodo del tiempo transcurrido desde la última copia
              de seguridad.
            </p>
            <p>
              Los servicios ofertados, excepto los servicios específicos de
              backup, no incluyen la reposición de los contenidos conservados en
              las copias de seguridad realizadas por
              https://elbosqueencantadoburgos.com, cuando esta pérdida sea
              imputable al usuario; en este caso, se determinará una tarifa
              acorde a la complejidad y volumen de la recuperación, siempre
              previa aceptación del usuario.
            </p>
            <p>
              La reposición de datos borrados sólo está incluida en el precio
              del servicio cuando la pérdida del contenido sea debida a causas
              atribuibles a https://elbosqueencantadoburgos.com
            </p>
            <strong>Comunicaciones comerciales</strong>
            <p>
              En aplicación de la LSSI. https://elbosqueencantadoburgos.com no
              enviará comunicaciones publicitarias o promocionales por correo
              electrónico u otro medio de comunicación electrónica equivalente
              que previamente no hubieran sido solicitadas o expresamente
              autorizadas por los destinatarios de las mismas.
            </p>
            <p>
              En el caso de usuarios con los que exista una relación contractual
              previa, https://elbosqueencantadoburgos.com sí está autorizado al
              envío de comunicaciones comerciales referentes a productos o
              servicios de https://elbosqueencantadoburgos.com que sean
              similares a los que inicialmente fueron objeto de contratación con
              el cliente.
            </p>
            <p>
              En todo caso, el usuario, tras acreditar su identidad, podrá
              solicitar que no se le haga llegar más información comercial a
              través de los canales de Atención al Cliente.
            </p>
          </blockquote>
        </div>
      </div>
      <Footer />
    </div>
  );
}
