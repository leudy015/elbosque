import GoogleMapReact from "google-map-react";

export default function Maps(props: any) {
  const { height, width, lat, lgn, title } = props;
  const mapObj = {
    center: {
      lat: 42.3428487,
      lng: -3.6979873,
    },
    city: "",
  };

  const renderMarkers = (map: any, maps: any) => {
    new maps.Marker({
      position: mapObj.center,
      map,
      title: title,
    });
  };

  return (
    <div style={{ height: height, width: width }}>
      <GoogleMapReact
        yesIWantToUseGoogleMapApiInternals={true}
        bootstrapURLKeys={{ key: "AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA" }}
        center={mapObj.center}
        defaultZoom={17}
        onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
      />
    </div>
  );
}
