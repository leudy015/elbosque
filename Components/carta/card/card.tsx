import React from "react";
import Link from "next/link";
import styles from "./card.module.css";

export default function Card(props: any) {
  const { data } = props;
  return (
    <>
      <Link href={data.slug === "algo-con-uvas" ? `/vinos/${data.slug}` : `/${data.slug}`}>
        <a className={styles.card}>
          <h2>{data.name}</h2>
        </a>
      </Link>
    </>
  );
}
