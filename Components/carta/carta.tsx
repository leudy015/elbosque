import React, { useState, useEffect } from "react";
import styles from "./carta.module.css";
import data from "../../data.json";
import Card from "./card/card";

export default function carta() {
  return (
    <div className={styles.content}>
      <h1>Nuestra carta</h1>
      <div className={styles.contentCarta}>
        {data.category.map((data, i) => {
          return <Card key={i} data={data} />;
        })}
      </div>
    </div>
  );
}
