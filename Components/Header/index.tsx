import React from "react";
import Link from "next/link";
import styles from "./header.module.css";
import Image from "next/image";

export default function Header() {
  return (
    <div className={styles.header}>
      <Link href="/">
        <a>
          <Image width={800} height={280} src="/images/logo.png" alt="El Bosque Encantado" />
        </a>
      </Link>
    </div>
  );
}
