import React from "react";
import styles from "./footer.module.css";
import {
  InstagramOutlined,
  FacebookOutlined,
  TwitterOutlined,
} from "@ant-design/icons";
import Link from "next/link";

export default function Footer() {
  return (
    <div className={styles.footer}>
      <div>
        <h1>El Bosque Encantado: Tu rincón perfecto para crear momentos</h1>
        <div className={styles.links}>
          <Link href="/politica-de-privacidad">Política de privacidad </Link>
          <Link href="/cookies">Política de cookies</Link>
        </div>
        <div style={{ textAlign: "center", marginTop: 50 }}>
          <a
            href="https://www.instagram.com/elbosqueencantado"
            target="_blank"
            rel="noreferrer"
          >
            <InstagramOutlined
              style={{ color: "#cc6c9c", fontSize: 30, marginRight: 15 }}
            />
          </a>
          <a
            href="https://www.facebook.com/cafe.pub.elbosqueencantado.burgos/"
            target="_blank"
            rel="noreferrer"
          >
            <FacebookOutlined
              style={{ color: "#cc6c9c", fontSize: 30, marginRight: 15 }}
            />
          </a>
          <a
            href="https://twitter.com/el_bosque_"
            target="_blank"
            rel="noreferrer"
          >
            <TwitterOutlined style={{ color: "#cc6c9c", fontSize: 30 }} />
          </a>
        </div>
        <p
          style={{
            color: "white",
            textAlign: "center",
            marginTop: 50,
            fontSize: 12,
          }}
        >
          © {new Date().getFullYear()} El Bosque Encantado Burgos
        </p>
      </div>
    </div>
  );
}
