import React, { useEffect, useState } from "react";
import styles from "./index.module.css";
import data from "../../data.json";
import { useRouter } from "next/router";
import Head from "next/head";
import Footer from "../../Components/Footer/footer";
import { CaretRightOutlined } from "@ant-design/icons";
import Link from "next/link";
import Image from "next/image";
import { Modal } from "antd";
import { DownOutlined, UpOutlined } from "@ant-design/icons";

export default function Details(props: any) {
  const { slug } = props;
  const [Loading, setLoading] = useState(false);
  const [datas, setData] = useState<any>(null);
  const [modal, setModal] = useState<boolean>(false);
  const [show, setShow] = useState(false);
  const [title, setTitle] = useState("");
  const router = useRouter();

  const filter = data.category.filter((x) => x.slug == slug);

  useEffect(() => {
    if (filter.length === 0) {
      router.push("/");
    }
  }, [filter]);

  setTimeout(() => {
    setLoading(false);
  }, 1500);

  if (Loading) {
    return null;
  }

  const item = filter && filter[0];

  const viewMore = (items: any) => {
    setData(items);
    setModal(true);
  };

  const onCancel = () => {
    setData(null);
    setModal(true);
  };

  const spand = (title: string) => {
    setTitle(title);
    setShow(!show);
  };

  return (
    <>
      <div className={styles.content_details}>
        <Head>
          <title>{item && item.name} | El Bosque Encantado</title>
          <meta
            name="description"
            content="El bosque encantado Burgos, el bar de moda tu ricón perfecto para crear momentos"
          />
        </Head>
        <div className="bg_header">
          <Link href="/">
            <a>
              <Image
                width={800}
                height={250}
                src="/images/logo.png"
                alt="El Bosque Encantado"
              />
            </a>
          </Link>
        </div>

        <div className={styles.info_cont}>
          <h1>{item && item.name}</h1>
          <span>{item && item.description}</span>

          <div className={styles.product_cont}>
            {item &&
              item.subCategory?.map((pro: any, i) => {
                return (
                  <div key={i}>
                    <div
                      style={{
                        cursor: "pointer",
                        display: "flex",
                        justifyContent: "flex-start",
                        alignItems: "center",
                      }}
                      onClick={() => spand(show ? "" : pro.type)}
                    >
                      <h2>{pro.type}</h2>{" "}
                      {
                        show && title === pro.type ? <UpOutlined
                        style={{
                          color: "#cc6c9c",
                          fontSize: 20,
                          marginLeft: 20,
                          marginTop: -10
                        }}
                      /> : <DownOutlined
                      style={{
                        color: "#cc6c9c",
                        fontSize: 20,
                        marginLeft: 20,
                        marginTop: -10
                      }}
                    />
                      }
                    </div>
                    {show && title === pro.type ? (
                      <>
                        {pro &&
                          pro.item?.map((prods: any, ix: any) => {
                            return (
                              <div key={ix}>
                                <h3>{prods.tipo}</h3>
                                {prods &&
                                  prods.products.map((p: any, y: any) => {
                                    return (
                                      <ul key={y}>
                                        <li>
                                          <div>
                                            <p>
                                              <CaretRightOutlined
                                                style={{
                                                  color: "#cc6c9c",
                                                  fontSize: 20,
                                                }}
                                              />
                                              {p.name}
                                            </p>
                                            <div style={{ marginLeft: 20 }}>
                                              {p.description ? (
                                                <>
                                                  <span>{p.description}</span>
                                                  <br />
                                                </>
                                              ) : null}
                                              {p.price_cup ? (
                                                <>
                                                  <br />
                                                  <span
                                                    style={{
                                                      color: "#cc6c9c",
                                                    }}
                                                  >
                                                    {p.price_cup}
                                                  </span>
                                                </>
                                              ) : null}
                                              {p.price ? (
                                                <>
                                                  <br />
                                                  <span
                                                    style={{
                                                      color: "#cc6c9c",
                                                    }}
                                                  >
                                                    {p.price}
                                                  </span>
                                                </>
                                              ) : null}
                                            </div>
                                          </div>
                                          <button
                                            className={styles.more}
                                            onClick={() => viewMore(p)}
                                          >
                                            Más información
                                          </button>
                                        </li>
                                      </ul>
                                    );
                                  })}
                              </div>
                            );
                          })}
                      </>
                    ) : null}
                  </div>
                );
              })}
          </div>
        </div>
        {datas ? (
          <Modal
            title={datas.name}
            visible={modal}
            onCancel={onCancel}
            footer={null}
          >
            <div
              dangerouslySetInnerHTML={{ __html: datas.large_description }}
            />
          </Modal>
        ) : null}
        <Footer />
      </div>
      <style jsx>
        {`
          .bg_header {
            width: 100%;
            height: 250px;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            position: relative;
            text-align: center;
            justify-content: center;
            align-items: center;
            display: flex;
            background-color: #cc6c9c;
          }

          .bg_header img {
            width: 50%;
          }

          @media (max-width: 768px) {
            .bg_header img {
              width: 350px;
            }
          }
        `}
      </style>
    </>
  );
}
