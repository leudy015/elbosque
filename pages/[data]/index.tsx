import React from "react";
import DetailsCarta from "../../Components/DetailsCarta";
import { useRouter } from "next/router";

export default function Details() {
  const router = useRouter();
  const { data } = router.query;

  return <DetailsCarta slug={data} />;
}
