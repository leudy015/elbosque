import React from "react";
import DetailsCarta from "../../Components/DetailsCartaVinos";
import { useRouter } from "next/router";

export default function Details() {
  const router = useRouter();
  const { vinos } = router.query;

  return <DetailsCarta slug={vinos} />;
}
