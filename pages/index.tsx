import React from "react";
import type { NextPage } from "next";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import Hero from "../Components/Hero";
import Carta from "../Components/carta";
import Contact from "../Components/Contacto";
import Footer from "../Components/Footer/footer";
import CookieConsent from "react-cookie-consent";

const Home: NextPage = () => {

  return (
    <div>
      <Head>
        <title>El Bosque Encantado</title>
        <meta
          name="description"
          content="El bosque encantado Burgos, el bar de moda tu ricón perfecto para crear momentos"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.container}>
        <Hero />
      </div>
      <Carta />
      <Contact />
      <Footer />
      <CookieConsent
        location="bottom"
        buttonText="Aceptar"
        cookieName="myAwesomeCookieName2"
        style={{
          background: "#eeeeee",
          color: "black",
        }}
        buttonStyle={{
          color: "white",
          fontSize: "14px",
          backgroundColor: "#cc6c9c",
          borderRadius: 10,
          width: 150,
          outline: "none",
        }}
        expires={150}
      >
        Este sitio web utiliza cookies para garantizar que obtenga la mejor
        experiencia en nuestro sitio web.{" "}
        <span style={{ fontSize: "10px" }}>
          Más detalles en https://elbosqueencantadoburgos.com/cookies
        </span>
      </CookieConsent>
    </div>
  );
};

export default Home;
